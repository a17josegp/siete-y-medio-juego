package sieteymedio.recursos;

public class Carta {

    //atributos
    private String palo;
    private int valor;
    private String id;
    //constructor
    public Carta() {
    }

    public Carta(String pPalo, int pValor) {
        this.palo = pPalo;
        this.valor = pValor;
        this.id = this.palo + this.valor;
    }

    //getter &  setter
    public String getPalo() {
        return palo;
    }

    public void setPalo(String pPalo) {
        this.palo = pPalo;
    }
    
    public String getId(){
        return id;
    }
    
    public int getValor() {
        return this.valor;
    }

    public void setValor(int pValor) {
        this.valor = pValor;
    }

    //metodos
    public float calcularValor() {
        float v1 = 0.0f;
        if (this.valor < 8) {
            v1 = this.valor;
        } else if (this.valor >= 8 && this.valor <= 10) {
            v1 = 0.5f;
        }
        return v1;
    }

    @Override
    public String toString() {

        if (this.getValor() < 8) {
            return this.getValor() + " de " + this.getPalo();
        } else if (this.getValor() == 8) {
            return "S de " + this.getPalo();
        } else if (this.getValor() == 9) {
            return "C de " + this.getPalo();
        } else if (this.getValor() == 10) {
            return "R de " + this.getPalo();
        }
        return null;

    }

}
