package sieteymedio.recursos;

import java.util.Arrays;
import sieteymedio.recursos.Carta;

import java.util.Random;
import sieteymedio.GameController;
import sieteymedio.Jugador;

/**
 *
 * @author cisco
 */
public class Baraja {
    //atributos
    public static final int TOTAL_CARTAS_BARAJA=40;
    private Carta[] cartas;
    private Carta[] cartasDadas;
    private int posicionCarta;

    //constructor
    public Baraja() {
        this.cartas = new Carta[TOTAL_CARTAS_BARAJA];
        this.posicionCarta = 0;
    }

    //getter & setter
    public Carta[] getCartas() {
        return cartas;
    }

    public void setCartas(Carta[] cartas) {
        this.cartas = cartas;
    }

    public Carta[] getCartasDadas() {
        return cartasDadas;
    }

    public void setCartasDadas(Carta[] pCartasDadas) {
        this.cartasDadas = pCartasDadas;
    }
    public int getPosicion(){
        return posicionCarta;
    }
    public void setPosicion(int pPosicion){
        this.posicionCarta= pPosicion;
    }
    //metodos
    /**
     * Desordena aleatoriamente las cartas de la baraja
     *
     * @return
     */
    public Carta[] barajaCartas() {
        this.setPosicion(0);
        Random rdm = new Random();
        for (int i = 0; i < cartas.length; i++) {
            int posicionRandom = rdm.nextInt(cartas.length);
            Carta lista = cartas[i];
            cartas[i] = cartas[posicionRandom];
            cartas[posicionRandom] = lista;
        }
        
        return cartas;
    }

    /**
     * Devuelve la relacion de cartas que han salido de la baraja
     *
     * @return
     */
    public Carta[] cartasDadas() {
        Carta[]  lista = new Carta[posicionCarta];
        
        for (int i = 0 ; i < posicionCarta ; i++) {
            lista[i]= cartas[i];
        }
        cartasDadas = lista;
        return cartasDadas;    
    }

    /**
     * Devuelve la relacion de cartas que siguen en la baraja
     *
     * @return
     */
    public Carta[] cartasMazo() {
        Carta[] lista = new Carta[cartas.length - posicionCarta];
        int cont = 0;
        //me incrementa la posicion global??
        int i= cartas.length;
         for (; i > posicionCarta; i--) {
            lista[cont]= cartas[cont];
            cont ++;
        }
        cartas = lista;
        return cartas;
    }
    /**
     * Devuelve la carta que esta en la posicion indicada
     * @return 
     */
    public Carta daUnaCarta(){
        Carta unaCarta = cartas[posicionCarta++];
//        posicionCarta++;

        return unaCarta;
    }    
    
    /**
     * Saca el numero indicado de cartas del mazo
     *
     * @param pNumero
     * @return
     */
    public Carta[] daCartas(int pNumero) {
        
        Carta[] cartasDar = new Carta[pNumero];
        
        for (int i = 0; i <cartasDar.length; i++) {
            cartasDar[i]= daUnaCarta();
        }
        return cartasDar;        
    }

    /**
     * Devuelve todas las cartas al mazo y baraja.
     */
    public void reiniciaBaraja() {
        crearMazo();
        barajaCartas();
    }

    /**
     * Crea el mazo con todos los palos y cartas tamaño total 40, por palo 10
     */
    public Carta[] crearMazo() {
        Carta[] mazo = new Carta[TOTAL_CARTAS_BARAJA];
        for (int i = 0, j = 10, k = 20, l = 30; i < 10 && j < 20 && k < 30 && l < 40; i++, j++, k++, l++) {
            mazo[i] = new Carta("Bastos", i + 1);
            mazo[j] = new Carta("Espadas", i + 1);
            mazo[k] = new Carta("Oros", i + 1);
            mazo[l] = new Carta("Copas", i + 1);
        }
        this.cartas = mazo;
        return this.cartas;
    }
}
