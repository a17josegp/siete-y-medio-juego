/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sieteymedio;

import java.util.Arrays;
import java.util.Scanner;
import sieteymedio.recursos.Baraja;
import sieteymedio.recursos.Carta;

/**
 *
 * @author cisco
 */
public class GameController {

    // atributos
    private Baraja baraja = new Baraja();
//    private Baraja[] mazo = new Baraja[40];
    private Jugador jugador;
    Jugador p1 = new Jugador("fran");

    Jugador p2 = new Jugador("Pablo");
    private Jugador[] jugadores = {p1, p2};

    // constructor
    // getters & setters
    // metodos
    // DINAMICA JUEGO
    /**
     * juega la mano de la banca por la apuesta indicada
     *
     * @param pApuesta
     */
    public void juegaMano(int pApuesta) {
        Jugador banca = new Jugador("Banca", 999999);
//        this.jugadores[1]= banca;
        this.jugadores[1] = banca;
//        baraja.daCartas(2);
        for (Carta daCarta : baraja.daCartas(2)) {
           jugadores[1].nuevaCarta(daCarta);
        }
        message("la " +jugadores[1].getNombre() + " tiene las siguientes cartas:\n" + Arrays.toString(jugadores[1].getCartas()));
        //TODO:
        //calcular valor cartas banca y comparar con jugador

        boolean ganar = true;
        do {
            float sumaValorCartasBanca = 0.0f;
            float sumaValorCartasJugador = 0.0f;
            for (Carta cartaBanca : jugadores[1].getCartas()) {
                if (cartaBanca == null); 
                else sumaValorCartasBanca += cartaBanca.calcularValor();                
            }
            for (Carta cartaJugador : jugadores[0].getCartas()) {
                if (cartaJugador == null); 
                else sumaValorCartasJugador += cartaJugador.calcularValor();                
            }
            if (sumaValorCartasBanca == 7.5) {
                jugadores[0].actualizarCredito(-pApuesta);
                message("Valor de la mano de la banca: " + sumaValorCartasBanca + ". SIETE Y MEDIA!! La banca se lo lleva todo.\nTotal de la apuesta = " + pApuesta + "\nTe queda un saldo de " + jugadores[0].getCreditos());
                message("Cartas que quedan el la baraja " + baraja.cartasMazo().length);          
//                message("Cartas que se han repartido: "+ baraja.cartasDadas().length + " => " + Arrays.toString(baraja.cartasDadas()) );
                // nueva mano o cerrar programa
                jugarOtraVez();
            } else if (sumaValorCartasBanca == sumaValorCartasJugador) {
                jugadores[0].actualizarCredito(-pApuesta);

                message("Valor de la mano de la banca: " + sumaValorCartasBanca + ". En caso de empate la banca gana.\nTotal de la apuesta = " + pApuesta + "\nTe queda un saldo de " + jugadores[0].getCreditos());
                message("Cartas que quedan el la baraja " + baraja.cartasMazo().length);
                message("Cartas que se han repartido: "+ baraja.cartasDadas().length + " => " + Arrays.toString(baraja.cartasDadas()) );
                // nueva mano o cerrar programa
                jugarOtraVez();
            } else if (sumaValorCartasBanca < sumaValorCartasJugador) {
                message("El valor de la mano de la banca es " + sumaValorCartasBanca + ", necesita quitar otra carta.");
                this.jugadores[1].nuevaCarta(baraja.daCartas(1)[0]);
//                message("Se han repartido las siguientes cartas para " + jugadores[1].getNombre() + "\n" + Arrays.toString(jugadores[1].getCartas()));
                ganar = false;
            } else if (sumaValorCartasBanca >= 8) {
                jugadores[0].actualizarCredito(pApuesta * 2);
                message("Valor de la mano de la banca: " + sumaValorCartasBanca + ", La banca pierde.\n" + jugadores[0].getNombre() + " has ganado " + pApuesta + " créditos que se suman a tu saldo\nSaldo: " + jugadores[0].getCreditos());
                message("Cartas que quedan el la baraja " + baraja.cartasMazo().length);
//                message("Cartas que se han repartido: "+ baraja.cartasDadas().length + " => " + Arrays.toString(baraja.cartasDadas()) );
                // nueva mano o cerrar programa
                jugarOtraVez();
            }
        } while (ganar != true);

    }

    /**
     * reinicia la baraja, la mano del jugador e inicia otra mano.
     */
    public void nuevaPartida() {
        this.jugadores[0].reiniciaMano();
        baraja.reiniciaBaraja();
        manoInicial();
    }

    /**
     * devuelve las primeras dos cartas que se entregan al iiniciar la partida,
     * y continua a evaluarMano.
     */
    public void manoInicial() {
        Scanner s = new Scanner(System.in);
      jugadores[0].reiniciaMano();
        message(jugadores[0].getNombre() + " introduce tu apuesta: ");
        int apuesta = s.nextInt();

       jugadores[0].actualizarCredito(-apuesta);
        System.out.println(this.jugadores[0].getCreditos());
//        baraja.daCartas(2);
        for (Carta daCarta : baraja.daCartas(2)) {
           jugadores[0].nuevaCarta(daCarta);
//            jugadores[0].nuevaCarta(baraja.daCartas(2)[i]);
//            System.out.println(Arrays.toString(baraja.daCartas(2)));
        }
        message("Se te han repartido las siguientes cartas:\n" + Arrays.toString(jugadores[0].getCartas()));
        evaluaMano(apuesta);
    }

    /**
     * evalua la puntuacion de cada mano y ofrece posibilidad de plantarse o
     * pedir carta
     *
     * @param pApuesta
     */
    public void evaluaMano(int pApuesta) {
        Scanner s = new Scanner(System.in);
        float sumaValorCartas = 0.0f;

        for (Carta carta : this.jugadores[0].getCartas()) {
            if (carta == null) ; else {
                sumaValorCartas += carta.calcularValor();
            }
        }
        if (sumaValorCartas == 7.5) {
            message("ENHORABUENA, SIETE Y MEDIO!! HAS GANADO " + (pApuesta * 2) + " creditos que se ingresan en tu saldo.");
            juegaMano(pApuesta);
            this.jugadores[0].actualizarCredito((pApuesta * 2) + pApuesta);
            message("tu saldo actual es de " + this.jugadores[0].getCreditos());
            this.jugadores[0].reiniciaMano();
            // nueva mano o cerrar programa
            jugarOtraVez();

        } else if (sumaValorCartas < 8) {
            message("Tu jugada vale " + sumaValorCartas + "\nQuieres pedir carta (1) o plantarte (2)?");
            int pedirPlantar = Integer.parseInt(s.nextLine());
            if (pedirPlantar == 1) {
                pedirCartas(pApuesta);
            } else {
                message("Tu jugada vale " + sumaValorCartas + "\nComprobemos que mano tiene la banca");
                juegaMano(pApuesta);

            }
        } else {
//            jugadores[0].actualizarCredito(-pApuesta + pApuesta);
            message("Has perdido!\nTotal de lo apostado = " + pApuesta + "\nTe queda un saldo de " + jugadores[0].getCreditos() + " créditos");
//            message("Cartas que quedan en el mazo: "+Arrays.toString(baraja.cartasMazo())); 
//                message("Se han repartid en esta partida estas cartas: "+Arrays.toString(baraja.cartasDadas()));
            // nueva mano o cerrar programa
            jugadores[0].reiniciaMano();
            jugarOtraVez();

        }
    }

    /**
     * Pregunta si se quiere volver a jugar (y/n) y -> nuevaPartida() n ->
     * mainMenu()
     */
    public void jugarOtraVez() {
        message("Quieres jugar otra mano? \n1.- continuar.\n0.- Salir");
        Scanner s = new Scanner(System.in);
        int otraMano = Integer.parseInt(s.nextLine());
        switch (otraMano) {
            case 1:
                manoInicial();
                break;
            case 0:
                mainMenu();
                break;
            default:
                message("No es una opcion válida, para continuar pulsa '1' y para salir '0'");
                break;
        }
    }

    /**
     * Reparte una carta nueva y evalua()
     *
     * @param pApuesta
     */
    public void pedirCartas(int pApuesta) {
        Scanner s = new Scanner(System.in);
        int totalApuesta = pApuesta;
//        System.out.println("Se han repartido " + Arrays.toString( baraja.cartasDadas()));
//        System.out.println("Quedan en la baraja las siguientes cartas " + Arrays.toString(baraja.cartasMazo()));

        message(jugadores[0].getNombre() + " introduce tu apuesta:");
        int apuesta = s.nextInt();
        totalApuesta += apuesta;
        jugadores[0].actualizarCredito(-pApuesta);
        jugadores[0].nuevaCarta(baraja.daCartas(1)[0]);
        message("Se te han repartido las siguientes cartas:\n" + Arrays.toString(jugadores[0].getCartas()));
        evaluaMano(totalApuesta);
    }
    //FIN DINAMICA JUEGO

    // MENUS
    public String verDatosJugador() {
        return "\nDatos del jugador:\n__________________\n" + jugadores[0].toString();

    }

    /**
     * muestra el menu principal con diferentes opciones
     */
    public void mainMenu() {
        //GameController gc= new GameController();
        Scanner s = new Scanner(System.in);
        String salir;

        do {
            message("Menu Principal:\n________________\n\n1.- Reglas del juego\n2.- Crear Jugador\n3.- Iniciar partida\n4.- Ver Datos jugador.\n5.- Salir.");

            int opcion = s.nextInt();
            switch (opcion) {
                case 1:
                    reglas();
                    break;
                case 2:
                    creaJugador();
                    message("Se ha creado un nuevo jugador.\n" + verDatosJugador() + "\n");
                    break;
                case 3:
                    startGame();
                    break;
                case 4:
                    message(verDatosJugador());
                    break;
                case 5:
                    message("Estás seguro de que quieres abandonar el juego? y/n");
                    salir = s.nextLine();
                    break;
                default:
                    System.out.println("opcion invalida");
                    break;
            }
            salir = s.nextLine();
        } while ("5".equals(salir) || !"y".equals(salir));

    }

    /**
     * comienza el juego, crea mazo y lo baraja, crea jugador
     */
    public void startGame() {
        baraja.crearMazo();
        message("Mazo creado con exito: \n" + Arrays.toString(baraja.cartasMazo()));
        baraja.barajaCartas();
        System.out.println(baraja.getCartas());
        //TODO: como validar que el jugador este creado??
        message("Comienza la partida\n___________________\n");
        manoInicial();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GameController gc = new GameController();
        gc.mainMenu();
//        gc.baraja.crearMazo();
//
//        for (int i = 0; i < gc.baraja.daCartas(2).length; i++) {
//            gc.jugadores[0].nuevaCarta(gc.baraja.daCartas(4)[i]);
//        }
//
//        System.out.println(gc.baraja.getCartas().length);
//        System.out.println("mazo: " + Arrays.toString(gc.baraja.getCartas()));
//
//        System.out.println("Cartas Dadas: " + Arrays.toString(gc.baraja.cartasDadas()));
//        System.out.println("Cartas que quedan en el mazo: " + Arrays.toString(gc.baraja.cartasMazo()));
//
//        gc.jugadores[0].nuevaCarta(gc.baraja.daUnaCarta());
//        gc.jugadores[0].nuevaCarta(gc.baraja.daUnaCarta());
//        float valor = 0.0f;
//        for (int i = 0; i < gc.jugadores[0].getCartas().length; i++) {
//            valor += gc.jugadores[0].getCartas()[i].calcularValor();
//        }
//        System.out.println("Valor de las cartas es: " + valor);
//        System.out.println("Cartas del jugador: " + Arrays.toString(gc.jugadores[0].getCartas()));
//
//        System.out.println("JUGADOR 2");
//        gc.jugadores[1].nuevaCarta(gc.baraja.daUnaCarta());
//        gc.jugadores[1].nuevaCarta(gc.baraja.daUnaCarta());
//        
//        for (int i = 0; i < gc.jugadores[1].getCartas().length; i++) {
//            valor += gc.jugadores[1].getCartas()[i].calcularValor();
//        }
//        System.out.println("Valor de las cartas es: " + valor);
//        System.out.println("Cartas del jugador: " + Arrays.toString(gc.jugadores[1].getCartas()));
    }

    /**
     * devuelve un texto con las reglas del juego y el menu princila;
     */
    public static void reglas() {
        System.out.println("Las siete y media​ es un juego de naipes que utiliza la baraja española de cuarenta naipes.\n"
                + "El juego consiste en obtener siete puntos y medio, o acercarse a ello lo más posible.\n"
                + "Las cartas valen tantos puntos como su valor facial, excepto las figuras, que valen medio punto.\n"
                + "Uno de los jugadores ha de actuar de Banca. La banca se encarga del reparto de cartas a cada jugador y a sí misma.\n"
                + "Cada jugador apuesta y, por turno, puede pedir cartas a la banca.\n"
                + "Se puede plantar en cualquier momento si ha llegado a siete y media, o si está por debajo.\n"
                + "Cuando todos los jugadores han jugado, le toca el turno a la banca, que descubre su carta y juega, a la vista de todos, hasta plantarse o pasarse.\n"
                + "Gana todo aquel que tenga más puntos que la banca, sin pasar de siete y media.\n"
                + "El que tiene siete y media, cobra el doble de su apuesta y, en algunos lugares, se convierte en banca.\n"
                + "A igualdad de puntos, siempre gana la banca.\n");
        GameController gc = new GameController();

        gc.mainMenu();
    }

    /**
     * muestra un mensaje a traves de un un syso
     *
     * @param pMensaje
     */
    public static void message(String pMensaje) {
        System.out.println(pMensaje);
    }

    /**
     * crea un jugador para poder empezar la partida.
     *
     * @return jugador
     */
    public Jugador creaJugador() {
        Scanner s = new Scanner(System.in);

        message("Ingresa tu nombre");
        String nombre = s.nextLine();
        message("Con cuanto saldo quieres empezar?");
        int saldo = s.nextInt();
        if (saldo < 10) {
            jugador = new Jugador(nombre);
            return this.jugadores[0] = jugador;
        } else {
            jugador = new Jugador(nombre, saldo);
            return this.jugadores[0] = jugador;
        }
    }
}
