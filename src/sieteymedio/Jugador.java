/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sieteymedio;

import java.util.Arrays;
import sieteymedio.recursos.Baraja;
import sieteymedio.recursos.Carta;

/**
 *
 * @author cisco
 */
public class Jugador {

    //atributos
//    Baraja baraja;
    private Carta[] cartas;
    private String nombre;
    private int creditos;
    private static final int TOTAL_CARTAS_RECIBIDAS=14;

    //constructor
    public Jugador() {
    }

    public Jugador(String nombre) {
        this.cartas = new Carta[TOTAL_CARTAS_RECIBIDAS];
        this.nombre = nombre;
        this.creditos = 1000;
    }

    public Jugador(String nombre, int creditos) {
        this.cartas = new Carta[TOTAL_CARTAS_RECIBIDAS];
        this.nombre = nombre;
        this.creditos = creditos;
    }

    //getter & setter
    public Carta[] getCartas() {
        return cartas;
    }

    public void setCartas(Carta[] pCartas) {
        this.cartas = pCartas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    //metodos
    /**
     * Actualiza el valor del credito con la cantidad recibida (positiva o
     * negativa) y devuelve el credito actual.
     *
     * @param pApuesta
     * @return
     */
    public int actualizarCredito(int pApuesta) {
        this.creditos += pApuesta;
        return this.creditos;
    }

    /**
     * Añade una nueva carta a la lista del jugador
     *
     * @param pCarta
     * @return
     */
    public void nuevaCarta(Carta pCarta) {
        Carta[] lista = new Carta[cartas.length +1 ];
        lista[0]= pCarta;
        for (int i = 0; i < this.getCartas().length; i++) {
            lista[i + 1]= this.getCartas()[i];
        }
        cartas= lista;
    }

    /**
     * Devuelve las cartas al mazo (limpia mano del jugador)
     */
    public void reiniciaMano() {
        Carta[] lista = new Carta[0];
        this.setCartas(lista);
    }

    @Override
    public String toString() {
        return "Nombre=> " + nombre + "\nSaldo=> " + creditos + "\n";
    }

}
